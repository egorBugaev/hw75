const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());

app.post('/encode/', (req, res) => {
	res.send(Vigenere.Cipher(req.body.pass).crypt(req.body.text));
});
app.post('/decode/', (req, res) => {
	res.send(Vigenere.Decipher(req.body.pass).crypt(req.body.text));
});

app.listen(port, () => {
	console.log('We are live on ' + port);
});
