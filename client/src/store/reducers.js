import {CHANGE_FIELD, FETCH_DECODE_SUCCESS, FETCH_ENCODE_SUCCESS} from "./actionTypes";

const initialState = {
	encoded:'',
	decoded:'',
	pass:''
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ENCODE_SUCCESS:
			return {...state, encoded: action.data};
		case FETCH_DECODE_SUCCESS:
			return {...state, decoded: action.data};
		case CHANGE_FIELD:
			return {...state, [action.event.target.name]: action.event.target.value};
		default:
			return state;
	}
};

export default reducer;
