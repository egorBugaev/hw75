import {CHANGE_FIELD, FETCH_DECODE_SUCCESS, FETCH_ENCODE_SUCCESS} from "./actionTypes";
import axios from '../axios-api';

export const fetchEncodeSuccess = data => {
	return {type: FETCH_ENCODE_SUCCESS, data};
};
export const fetchDecodeSuccess = data => {
	return {type: FETCH_DECODE_SUCCESS, data};
};

export const change = (event) => {
	return {type: CHANGE_FIELD , event}
};

export const encode = (toEncode) => {
	return dispatch => {
		return axios.post('/encode', toEncode).then(
			response => dispatch(fetchEncodeSuccess(response.data))
		);
	};
};
export const decode = (toDecode) => {
	return dispatch => {
		return axios.post('/decode', toDecode).then(
			response => dispatch(fetchDecodeSuccess(response.data))
		);
	};
};
